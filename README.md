# ScanBill
Scan Bill module extract amount of money and classify the bill

## I. Install venv and create virtual environment
Link: https://help.dreamhost.com/hc/en-us/articles/115000695551-Installing-and-using-virtualenv-with-Python-3


Run the bellow command
````
    python3 -m pip install --upgrade pip
    pip3 install virtualenv
    which virtualenv
    which python3
    virtualenv venv_scanBill
    source venv_scanBill/bin/activate
    pip3 install --upgrade setuptools
    
    sudo apt-get install python3.7-dev
    # python3 -m pip install paddlepaddle==2.0.0 -i https://mirror.baidu.com/pypi/simple
    pip3 --no-cache-dir install -r requirements.txt
    python -m pip install python-dotenv
    pip3 --no-cache-dir install torch==1.9.0+cpu torchvision==0.10.0+cpu torchaudio==0.9.0 -f https://download.pytorch.org/whl/torch_stable.html
    pip3 install -U scikit-learn scipy matplotlib
    pip3 install paddlepaddle==2.0.0 paddleocr vietocr==0.3.2

````

## II. Run the scan bill module demon
````
    python3 scan_bill.py
````