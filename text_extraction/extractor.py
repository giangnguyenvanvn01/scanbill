import os
import re
import numpy as np
import json
from fb_duckling import Duckling
import logging
logging.basicConfig(filename='./log/text_extraction.log', filemode='w', level=logging.DEBUG)


class TotalMoneyExtractor():
    
    def __init__(self, file_config = "./text_extraction/config.json"):
        
        with open(file_config, 'r') as rf:
            self.config = json.loads(rf.read())  

        self.name = self.config["name"]
        path_total_vocal = self.config["path_total_vocal"]
        with open(path_total_vocal, "r") as rf:
            total_vocabs = rf.readlines()
        self.total_vocabs = [total_vocabs[i].strip() for i in range(len(total_vocabs))]
        # setting duckling
        self.duckling = Duckling(locale="vi_GB")
        self.valid_unit = ["VND"]
        self.valid_dim = ["amount-of-money", "number"]
        # setting distance between bouding boxes
        self.DEFAULT_DIS = self.config["DEFAULT_DIS"]

    
    def __search_total_vocab(self, scripts):
        for total_vocab in self.total_vocabs:
            for i, script in enumerate(scripts):
                script = script.lower()
                if script.find(total_vocab) != -1:
                    return i, total_vocab
        return -1, "NULL"
    
    
    def __filter_response_duckling(self, scripts):
        outputs = " __ ".join(script.replace(",", ".") for script in scripts)
        am_candidates = []
        am_indexes = []

        responses = self.duckling(outputs)
        if not len(responses):
            return am_indexes, am_candidates

        for res in responses:
            # check dim
            dim__ = res["dim"]
            if dim__ not in self.valid_dim:
                continue
            # check unit of money
            try:
                unit__ = res["value"]["unit"]
                if unit__ not in self.valid_unit:
                    continue
            except:
                pass
            # check body with ",", "."
            body__ = res["body"]
            if body__.find(".") == -1:
                continue
            am_candidates.append(res)
        return am_candidates


    def __get_top_amount_money(self, am_candidates):
        if len(am_candidates) == 0:
            return None
        amount_moneys = []
        for am_candidate in am_candidates:
            value__ = am_candidate["value"]["value"]
            amount_moneys.append(value__)
        arr = np.array(amount_moneys)
        top_am_candidate = am_candidates[arr.argsort()[-1]]
        top_total_money = top_am_candidate["value"]["value"]
        logging.debug("Top Amount: {}".format(top_total_money))
        return top_total_money

    

    def __compute_distance(self, total_vocab_bb__, am_candidate_bb__):
        y_center_total_vocab__ = total_vocab_bb__[1] + int((total_vocab_bb__[3] - total_vocab_bb__[1])/2)
        x_center_total_vocab__ = total_vocab_bb__[0] + int((total_vocab_bb__[2] - total_vocab_bb__[0])/2)

        y_center_am_candidate__ = am_candidate_bb__[1] + int((am_candidate_bb__[3] - am_candidate_bb__[1])/2)
        x_center_am_candidate__ = am_candidate_bb__[0] + int((am_candidate_bb__[2] - am_candidate_bb__[0])/2)

        # compute distance horizontally
        if y_center_am_candidate__ < total_vocab_bb__[1]:
            hor_distance = self.DEFAULT_DIS
        else:
            hor_distance = abs(y_center_total_vocab__ - y_center_am_candidate__)
        
        # compute distance vertically
        if x_center_am_candidate__ > total_vocab_bb__[2] or x_center_am_candidate__ < total_vocab_bb__[0]:
            ver_distance = self.DEFAULT_DIS
        else:
            ver_distance = abs(x_center_total_vocab__ - x_center_am_candidate__)    
        return hor_distance, ver_distance
    
    
    def __check_horizotal_distance(self, 
                                 am_candidates, 
                                 hor_distances):
        arr = np.array(hor_distances)
        if arr[arr.argsort()[0]] == self.DEFAULT_DIS: 
            return None
        return am_candidates[arr.argsort()[0]]

    
    def __check_vertical_distance(self, 
                                am_candidates, 
                                ver_distances):
        arr = np.array(ver_distances)
        if arr[arr.argsort()[0]] == self.DEFAULT_DIS: 
            return None
        return am_candidates[arr.argsort()[0]]
    
    
    def __indicate_amount_moneys(self,
                               data,
                               scripts,
                               total_vocab_index,
                               total_vocab, 
                               am_candidates):

        scripts__ = [script.replace(",", ".") for script in scripts]

        total_vocab_bb__ = data["boxes"][total_vocab_index]["bounding_box"]
        #         print(total_vocab_bb__)
        
        hor_distances = []
        ver_distances = []
        am_candidate_bbs__ = []
        for i, am_candidate in enumerate(am_candidates):
            hor_distance = self.DEFAULT_DIS
            ver_distance = self.DEFAULT_DIS

            body__ = am_candidate["body"]
            index = -1
            for j, script__ in enumerate(scripts__):
                if script__.find(body__) != -1:
                    index = j
            if index == -1:
                hor_distances.append(hor_distance)
                ver_distances.append(ver_distance)
                am_candidate_bbs__.append([])
                continue

            # check if total_vocab and am_candidate are same 
            if index == total_vocab_index:
                return am_candidate['value']['value'], am_candidate['value']['value']
            am_candidate_bb__ = data["boxes"][index]["bounding_box"]
            am_candidate_bbs__.append(am_candidate_bb__)

            # compute distance between two bounding box
            hor_distance, ver_distance = self.__compute_distance(am_candidate_bb__, total_vocab_bb__)

            hor_distances.append(hor_distance)
            ver_distances.append(ver_distance)
            
            #             print("----------------------")
            #             print(am_candidate_bb__)
            #             print(body__)
            #             print(hor_distance)
            #             print(ver_distance)

        
        hor_am_candidate = self.__check_horizotal_distance(am_candidates, hor_distances)
        ver_am_candidate = self.__check_vertical_distance(am_candidates, ver_distances)

        if hor_am_candidate != None:
            logging.debug(f"Horizontal: {hor_am_candidate['value']['value']}")
            hor_total_money = hor_am_candidate['value']['value']
        else: 
            hor_total_money = None

        if ver_am_candidate != None:
            logging.debug(f"Vertical: {ver_am_candidate['value']['value']}")
            ver_total_money = ver_am_candidate['value']['value']
        else: 
            ver_total_money = None
        return hor_total_money, ver_total_money

    
    def extract(self, data, scripts):
        total_vocab_index, total_vocab = self.__search_total_vocab(scripts)
        logging.debug("Key word: {}".format(scripts[total_vocab_index]))
        
        if total_vocab_index == -1:
            hor_total_money = None
            ver_total_money = None
        else:
            am_candidates = self.__filter_response_duckling(scripts)
            hor_total_money, ver_total_money = self.__indicate_amount_moneys(data,
                                                                       scripts,
                                                                       total_vocab_index,
                                                                       total_vocab, 
                                                                       am_candidates)
        top_total_money = self.__get_top_amount_money(am_candidates)

        return top_total_money, hor_total_money, ver_total_money