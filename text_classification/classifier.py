import os
from collections import OrderedDict
import json
import logging
logging.basicConfig(filename='./log/text_classification.log', filemode='w', level=logging.DEBUG)

class BillClassifer():
    def __init__(self, file_config = "./text_classification/config.json"):
        with open(file_config, 'r') as rf:
            self.config = json.loads(rf.read())  
        self.path_folder_txt = self.config["path_folder_txt"]
        

    def __load_common_words(self):
        common_words = {}
        for file_txt in os.listdir(self.path_folder_txt):
            name_field = file_txt.replace(".txt", "")
            path_file_txt = os.path.join(self.path_folder_txt, file_txt)
            with open(path_file_txt, "r") as rf:
                examples = rf.readlines()
                for example in examples:
                    example = example.strip()
                    example = example.lower()
                    if not len(example):
                        continue
                    try:
                        common_words[example] = name_field
                    except:
                        print("The common word is dubplicate!!!")
        return common_words
    

    def __get_common_words_from_label(self,
                                    common_words, 
                                    label = "shopping_market"):
        group_common_words = []
        for common_word, common_word_label in common_words.items(): 
            if common_word_label == label:
                group_common_words.append(common_word)
        return group_common_words

    def classify(self,
                scripts):
        
        label = "NULL"
        # convert from scripts to output with __ as split
        output = " __ ".join(script for script in scripts)
        
        # load common words for each label
        common_words = self.__load_common_words()

        # check if the bill is shopping market
        shopping_market_words = self.__get_common_words_from_label(common_words, 
                                                                  label = "shopping_market")

        for shopping_market_word in shopping_market_words:
            if output.find(shopping_market_word) != -1:
                return common_words[shopping_market_word]

        dict_counter = OrderedDict()
        for common_word in common_words.keys(): 
            if common_word in output:
                common_word_label = common_words[common_word]
                logging.debug(common_word)
                if common_word_label not in dict_counter:
                    dict_counter[common_word_label] = 1
                else:
                    dict_counter[common_word_label] += 1
        if len(dict_counter.keys()):
            label = max(dict_counter, key=dict_counter.get)
        return dict_counter, label