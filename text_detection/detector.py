from paddleocr import PaddleOCR, draw_ocr
from PIL import Image
# from matplotlib import pyplot as plt    
import json
import numpy as np
from PIL import Image, ImageDraw
from math import *
import os
import glob
import uuid
import logging
logging.basicConfig(filename='./log/text_detection.log', filemode='w', level=logging.DEBUG)

class TextDetector():
    def __init__(self, 
                file_config = "./text_detection/config.json") -> None:

        with open(file_config, 'r') as rf:
            self.config = json.loads(rf.read())  

        self.core_name = self.config["core_name"]
        self.lang = self.config["language"]
        self.detector = PaddleOCR(lang=self.lang)
        
        # status configuration
        self.save_bb_image_status = self.config["save_bb_image"]
        self.save_image_unique_name_status = self.config["save_image_unique_name"]

        self.tmp_output_folder_path_image = self.config["tmp_output_folder_path_image"]
        self.tmp_output_folder_path_json = self.config["tmp_output_folder_path_json"]

        if not os.path.exists(self.tmp_output_folder_path_image):
            os.makedirs(self.tmp_output_folder_path_image)
        if not os.path.exists(self.tmp_output_folder_path_json):
            os.makedirs(self.tmp_output_folder_path_json)
        
        self.tmp_image_folder = self.config["tmp_image_folder"]
        if not os.path.exists(self.tmp_image_folder):
            os.makedirs(self.tmp_image_folder)
            
        
        self.tmp_image_path, self.tmp_bb_image_path,  self.output_json  = "None", "None", "None"
        self.bb_filter_ratio = self.config["bb_filter_ratio"]
        self.bb_expand_ratio = self.config["bb_expand_ratio"]
        self.size_image = self.config["size_image"]
        self.degree_threshold = self.config["degree_threshold"]

        self.save_detection_result_as_json = self.config["save_detection_result_as_json"]
        self.save_detection_result_as_json = self.config["save_detection_result_as_json"]
        
        
    def save_tmp_image_as_unique__(self):
        '''
            Check status and setting the method to save the uploaded image
        '''
        if self.save_image_unique_name_status:            
            image_unique_name =  f"{uuid.uuid4().hex}.jpg"
            self.tmp_image_path = os.path.join(self.tmp_image_folder, image_unique_name)
            self.output_json = image_unique_name.replace("jpg", "json")
            image_unique_name = image_unique_name.replace(".jpg", "_bb.jpg")
            self.tmp_bb_image_path = os.path.join(self.tmp_image_folder, image_unique_name)
            
        else:
            self.tmp_image_path = self.config["tmp_image_path"]
            self.tmp_bb_image_path = self.config["tmp_bb_image_path"]
            self.output_json = "output.json"
        return self.tmp_image_path, self.tmp_bb_image_path, self.output_json

    
    def remove_small_bounding_box(self, bounding_boxes):
        '''
            Filter small bounding boxes
        '''
        high_bounding_box_list = []
        for bounding_box in bounding_boxes:
            top = bounding_box[0][1] 
            bottom = bounding_box[2][1]
            high_bounding_box = int(bottom - top)
            high_bounding_box_list.append(high_bounding_box)

        high_bounding_box_array = np.array(high_bounding_box_list)
        mean_high_bounding_box = np.mean(high_bounding_box_array)
        
        condition = high_bounding_box_array > mean_high_bounding_box * self.bb_filter_ratio
        bounding_boxes = np.array(bounding_boxes)
        bounding_boxes__ = bounding_boxes[condition, :]
        
        logging.debug(f"Mean of height bounding box: {mean_high_bounding_box} pixels")
        logging.debug(f"Number of bounding box: {len(bounding_boxes)}")
        logging.debug(f"Number of filtered bounding box: {len(bounding_boxes__)}")
        
        return bounding_boxes__
            
    def verify_bounding_box(self, image, bounding_box):
        '''
            Verify the values of bounding box which
            include top, bottom, left, right
        '''
        width, height = image.size
        [top, left, bottom, right] = bounding_box
        if top < 0: top = 0 
        if bottom > height - 1: bottom  = height - 1 
        if left < 0: left = 0 
        if right > width - 1: right  = width - 1 
        return [int(top), int(left), int(bottom), int(right)]

    def compute_angle__(self, bounding_box):
        '''
            Compute the angle of bouding_box horizontally
        '''
        [(x1, y1), _, _, (x2, y2)] = bounding_box
        angle = degrees(atan((x2 - x1) / (y2 - y1)))
        return angle

    def expand_bounding_box(self, image, bounding_boxes):
        '''
            Expand the bounding boxes with both horizontal and vertically
        '''
        expaned_bounding_boxes = []
        for bounding_box in bounding_boxes:
            left = min(bounding_box[0][0], bounding_box[3][0])
            top = min(bounding_box[0][1], bounding_box[1][1])
            right = max(bounding_box[1][0], bounding_box[2][0])
            bottom = max(bounding_box[2][1], bounding_box[3][1])
            height = bottom - top
            width = right - left

            angle = self.compute_angle__(bounding_box)
            logging.debug(f"Angle: {angle}")
            if abs(angle) >= self.degree_threshold:
                if angle > 0:
                    # logging.debug(self.bb_expand_ratio[0]/4)
                    top = top - int(height * (self.bb_expand_ratio[0]/8))
                    bottom = bottom + int(height * self.bb_expand_ratio[0])
                else:
                    top = top - int(height * self.bb_expand_ratio[0])
                    bottom = bottom + int(height * (self.bb_expand_ratio[0]/8))
            else:
                top = top - int(height * self.bb_expand_ratio[0])
                bottom = bottom + int(height * self.bb_expand_ratio[0])
            left = left - int(height * self.bb_expand_ratio[1])
            right = right + int(height * self.bb_expand_ratio[1])

            [top, left, bottom, right] = self.verify_bounding_box(image, [top, left, bottom, right])
            expaned_bounding_boxes.append([left, top, right, bottom])
        return expaned_bounding_boxes

    def draw_bounding_box(self, pil_image, bounding_boxes):
        '''
            Draw bouding_boxes in the image and save it 
        '''
        # create rectangle image
        draw_image = ImageDraw.Draw(pil_image)  
        
        for bounding_box in bounding_boxes:
            [left, top, right, bottom] = bounding_box
            shape = [(left, top), (right, bottom)]
            # draw_image.rectangle(bounding_box, fill ="# ffff33", outline ="red")
            draw_image.rectangle(bounding_box, outline ="red")
        pil_image.save(self.tmp_bb_image_path)
        
    def extract_bounding_box(self, pil_image, bounding_boxes):
        '''
            Extract bounding box image from original image
        '''
        bounding_boxes_images = []
        for bounding_box in bounding_boxes:
            [left, top, right, bottom] = bounding_box
            shape = [(left, top), (right, bottom)]
            bb_image = pil_image.crop((left, top, right, bottom))
            bounding_boxes_images.append(bb_image)
        return bounding_boxes_images
    
    def save_result(self, 
                    org_bounding_boxes, 
                    bounding_boxes, 
                    bounding_boxes_images,
                    save_bounding_box_image = False,
                    save_detection_result_as_json = False):
        '''
            Save result as json file
        '''
        path_files = glob.glob(os.path.join(self.tmp_output_folder_path_image, "*"))
        for path_file in path_files:
            os.remove(path_file)        
        path_output_files = []
        
        for i, bounding_boxes_image in enumerate(bounding_boxes_images):
            image_unique_name = os.path.basename(self.tmp_image_path)
            image_unique_name = image_unique_name.replace(".jpg", "_{:03d}.jpg".format(i))
            path_output_file = os.path.join(self.tmp_output_folder_path_image, image_unique_name)
            if save_bounding_box_image:
                try:
                    bounding_boxes_image.save(path_output_file)
                    logging.debug(f"** Save {path_output_file} is done")
                except Exception as e:
                    logging.error(f"** Error {path_output_file} {e}")
            path_output_files.append(path_output_file)

        # save result as dictionary
        result_as_dict = {}
        result_as_dict["original_image"] = self.tmp_image_path
        list_bounding_boxes = []
        for i, bounding_boxes_image in enumerate(bounding_boxes_images):
            dict_bounding_box = {}
            dict_bounding_box["index"] = i
            dict_bounding_box["path_file"] = path_output_files[i]
            dict_bounding_box["bounding_box"] = bounding_boxes[i]
            dict_bounding_box["org_bounding_box"] = org_bounding_boxes[i].tolist()
            list_bounding_boxes.append(dict_bounding_box)
        result_as_dict["boxes"] = list_bounding_boxes
        # write result as json file
        if self.save_detection_result_as_json:
            with open(os.path.join(self.tmp_output_folder_path_json, self.output_json), "w") as outfile: 
                json.dump(result_as_dict, outfile)
        return result_as_dict

    
    def detect(self, pil_image):
        '''
            detect() - the main function which create a pipe line to 
            process, predict image and save the result of the image 
        '''
        self.tmp_image_path, self.tmp_bb_image_path,  self.output_json  = self.save_tmp_image_as_unique__()
        
        # save the image as tmp image
        if self.size_image != [0, 0]:
            pil_image = pil_image.resize(self.size_image)
        pil_image.save(self.tmp_image_path)
        
        # text detection and get bounding box
        org_bounding_boxes = self.detector.ocr(self.tmp_image_path, rec=False)
        org_bounding_boxes = org_bounding_boxes[::-1]

        # filter small bounding box
        org_bounding_boxes = self.remove_small_bounding_box(org_bounding_boxes)

        # expand bounding box
        bounding_boxes = self.expand_bounding_box(pil_image, org_bounding_boxes)
        
        # extract bounding box image from input image
        bounding_boxes_images = self.extract_bounding_box(pil_image, bounding_boxes)

        # draw bounding box to the input image
        if self.save_bb_image_status == True:
            self.draw_bounding_box(pil_image, bounding_boxes)
        
        # save the detection result
        result_as_dict = self.save_result(org_bounding_boxes, bounding_boxes, bounding_boxes_images)
        logging.debug("Text Detection is done")
        return bounding_boxes, bounding_boxes_images, result_as_dict



