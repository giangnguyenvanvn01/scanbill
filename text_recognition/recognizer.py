from paddleocr import PaddleOCR, draw_ocr
from PIL import Image, ImageEnhance
from matplotlib import pyplot as plt    
import json
import numpy as np
from PIL import Image, ImageDraw
from math import *
import glob
import os
import logging
logging.basicConfig(filename='./log/text_recognition.log', filemode='w', level=logging.DEBUG)
from vietocr.tool.predictor import Predictor
# from vietocr.tool.config import Cfg
import yaml


class TextRecognizer():
    def __init__(self, 
                file_config = "./text_recognition/config.json") -> None:

        with open(file_config, 'r') as rf:
            self.config = json.loads(rf.read())  
        self.core_name = self.config["core_name"]
        
        with open('./text_recognition/config-vgg-transformer.yml') as file:
            # The FullLoader parameter handles the conversion from YAML
            # scalar values to Python the dictionary format
            config = yaml.load(file, Loader=yaml.FullLoader)
                # config = Cfg.load_config_from_name('vgg_transformer')
                # config['weights'] = self.config["weight"]
                # config['cnn']['pretrained']= self.config["pretrained"]
                # config['device'] = self.config["device"]
                # config['predictor']['beamsearch'] = self.config["beamsearch"]
        self.recognizer = Predictor(config)

        self.load_input_image_status = False
        self.tmp_output_folder_path_json = self.config["tmp_output_folder_path_json"]
        self.tmp_output_folder_path_image = self.config["tmp_output_folder_path_image"]
        self.tmp_output_folder_path_image_recog = self.config["tmp_output_folder_path_image_recog"]
        self.tmp_output_folder_path_json_recog = self.config["tmp_output_folder_path_json_recog"]
        if not os.path.exists(self.tmp_output_folder_path_image_recog):
            os.makedirs(self.tmp_output_folder_path_image_recog)
        if not os.path.exists(self.tmp_output_folder_path_json_recog):
            os.makedirs(self.tmp_output_folder_path_json_recog)
        self.constrast_factor = self.config["constrast_factor"]


        self.save_result_as_json = self.config["save_result_as_json"]
        self.save_result_as_image = self.config["save_result_as_image"]


    def load_detection_json(self, json_file):
        '''
            Load text detection result as dictionary
        '''
        with open(os.path.join(self.tmp_output_folder_path_json, json_file)) as rf:
            data = json.load(rf)
        return data

    def extract_bounding_box__(self, pil_image, bounding_box):
        '''
            Extract bounding box image from original image
        '''
        [left, top, right, bottom] = bounding_box
        shape = [(left, top), (right, bottom)]
        bounding_box_image = pil_image.crop((left, top, right, bottom))
        return bounding_box_image

    def save_result__(self, data):
        image_unique_name = os.path.basename(self.tmp_image_path)
        self.output_json = image_unique_name.replace("jpg", "json")
        with open(os.path.join(self.tmp_output_folder_path_json_recog, self.output_json), "w") as outfile: 
            json.dump(data, outfile)


    def draw_result(self, json_file = None, data = None, save_status = True):
        
        if json_file != None:
            data = self.load_detection_json(json_file)
        elif data == None:
            return []
        self.tmp_image_path = data["original_image"]
        org_image = Image.open(self.tmp_image_path)

        boxes = data["boxes"]
        bounding_boxes = []
        scripts = []
        scores = None
        for i, box in enumerate(boxes):
            index = box["index"]
            path_file = box["path_file"] 
            org_bounding_box = box["org_bounding_box"]
            bounding_box = box["bounding_box"]
            script = box["script"]
            bounding_boxes.append(np.array(org_bounding_box))
            scripts.append(script)

        im_show = draw_ocr(org_image, bounding_boxes, scripts, scores, font_path='./doc/fonts/latin.ttf')
        im_show = Image.fromarray(im_show)
        if save_status:
            # save the result image
            # plt.imshow(im_show)
            # plt.show()
            image_unique_name = os.path.basename(self.tmp_image_path)
            im_show.save(os.path.join(self.tmp_output_folder_path_image_recog, image_unique_name))
        return im_show

    def recognize(self, json_file = None, data = None, show_status = True):
        '''
            recognize() - the main function which create a pipe line to 
            process, predict text detection result from json file and
            save the result of the image 
        '''
        
        if json_file != None:
            data = self.load_detection_json(json_file)
        elif data == None:
            return []
        self.tmp_image_path = data["original_image"]
        org_image = Image.open(self.tmp_image_path)
        boxes = data["boxes"]
        scripts = []
        for i, box in enumerate(boxes):
            # extract bounding box image from original image
                    # index = box["index"]
            path_file = box["path_file"] 
            bounding_box = box["bounding_box"]
            if os.path.exists(path_file):
                bounding_box_image = Image.open(path_file)
            else:
                bounding_box_image = self.extract_bounding_box__(org_image, bounding_box)

            # image brightness enhancer
            enhancer = ImageEnhance.Contrast(bounding_box_image)
            self.constrast_factor = 2 #gives original image
            bounding_box_image = enhancer.enhance(self.constrast_factor)
            
            # --------------------------------------------------------
            # recognize image from vietocr
            script = self.recognizer.predict(bounding_box_image)
            scripts.append(script)
            # --------------------------------------------------------

            # show bouding box images
            if show_status:
                # plt.imshow(bounding_box_image)
                # plt.show()
                logging.debug(script) 
            
            # save the result
            box["script"] = script
        data["boxes"] = boxes
        if self.save_result_as_json:
            self.save_result__(data)
        if self.save_result_as_image :
            self.draw_result(data = data, save_status = True)

        return data, scripts