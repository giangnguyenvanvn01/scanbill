
from PIL import Image
import os
import numpy
import cv2
import json
import csv
import regex as re

from text_detection.detector import TextDetector
from text_recognition.recognizer import TextRecognizer
from text_extraction.extractor import TotalMoneyExtractor
from text_classification.classifier import BillClassifer




td = TextDetector()
tr = TextRecognizer()
tme = TotalMoneyExtractor()
bc = BillClassifer()

def recognize_from_path_image(path_image):
    
    image = Image.open(path_image).convert('RGB')
    _, _, result_as_dict = td.detect(image)

    data, scripts = tr.recognize(data=result_as_dict, show_status = False)        
    # print(scripts)
    output = ""
    for script in scripts:
        # remove ignored characters
        script__ = re.sub('[^A-Za-z0-9 ]+', '', script)
        output = output + ' ' + script__
    return data, scripts, output


def main(path_image):
    data, scripts, _ = recognize_from_path_image(path_image)
    
    top_total_money, hor_total_money, ver_total_money = tme.extract(data, scripts)
    # print(top_total_money, hor_total_money, ver_total_money)

    _, label = bc.classify(scripts)
    # print(label)

    result_classifer_and_extractor = {}
    result_classifer_and_extractor["Type of bill"] = label
    result_classifer_and_extractor["Total amount money candidate"] = [top_total_money, hor_total_money, ver_total_money]
    print(result_classifer_and_extractor)

if __name__ == '__main__':
    path_image = "./test/images/0001.png"
    main(path_image)
    

















